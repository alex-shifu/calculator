import numpy

def intro():
	
	print("WELCOME TO CALCULATOR")
	
	operations()
	
def operations():
	
	operationsMapper					 =	{
		1								 :	add,
		2								 :	substract,
		3								 :	multiply,
		4								 :	divide
	}
	
	for operationMap in operationsMapper.items():
		print("%i => %s" %(operationMap[0], operationMap[1].__name__.title()));
		
	operation							 =	int(input("Select operation: "))
	
	if operation not in operationsMapper:
		
		print("Invalid Operation selection.")
		
		exit(0)
	
	selectedOperation					 =	operationsMapper[operation]
	
	print("%s Operation" %(selectedOperation.__name__.title()))
	print("----------------------------------")
	
	selectedOperation()
	
	operations()
	
def takeInput(numberOfInputs, message):
	
	inputs								 =	[]
	
	for i in range(2):
		
		value							 =	input(message + " for input %i: " %(i+1))
		checkInput(value)
		
		inputs.append(float(value))
		
	return inputs
	
def checkInput(value):
	
	try:
		
		float(value)
		
	except ValueError:
		
		print("Values can only be INT or FLOAT")
		
		exit(0)
	
def add():
	
	values								 =	takeInput(2, "Enter value")
	
	result								 =	0
	
	for (index, value) in enumerate(values):
		
		result							 +=	value
		
	print("Result for %s: %s" %(",".join(numpy.array(values, dtype=str)), result))
	
def substract():
	
	values								 =	takeInput(2, "Enter value")
	
	result								 =	values[0]
	valuesAsString						 =	",".join(numpy.array(values, dtype=str))
	
	values.pop(0)
	
	for (index, value) in enumerate(values, start = 2):
		
		result							 -=	value
		
	print("Result for %s: %s" %(valuesAsString, result))
	
def multiply():
	
	values								 =	takeInput(2, "Enter value")
	
	result								 =	values[0]
	valuesAsString						 =	",".join(numpy.array(values, dtype=str))
	
	values.pop(0)
	
	for (index, value) in enumerate(values, start = 2):
		
		result							 *=	value
		
	print("Result for %s: %s" %(valuesAsString, result))
	
def divide():
	
	values								 =	takeInput(2, "Enter value")
	
	result								 =	values[0]
	valuesAsString						 =	",".join(numpy.array(values, dtype=str))
	
	values.pop(0)
	
	for (index, value) in enumerate(values, start = 2):
		
		result							 /=	value
		
	print("Result for %s: %s" %(valuesAsString, result))
	

if __name__ == "__main__":
	intro()